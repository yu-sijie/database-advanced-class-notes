# 存储过程语法

模板
~~~
create proc 存储过程名称

每个存储过程的参数个数	2100

as
begin

end
~~~

</br>

~~~sql
create proc proc_SelectStudentInfo
as
begin
    select * from StudentInfo
end

exec proc_SelectStudentInfo
~~~

单个
~~~sql
create proc proc_SelectStudentInfoWithPrameter
@name nvarchar(80)
as
begin 
    select * from StudentInfo
    where StudentName like @name
end
go

exec proc_SelectStudentInfoWithPrameter '%李%'
~~~

多个
~~~sql
create proc proc_SelectStudentInfoWithSomePrameter
@name nvarchar(80),
@code nvarchar(80)
as
begin
    select * from StudentInfo
    where StudentName like @name or StudentCode like @code
end
go

exec  proc_SelectStudentInfoWithSomePrameter '%李%','%1%' 
~~~

# 命名规范
~~~
句法：

存储过程的命名有这个的语法：
[proc] [MainTableName] By [FieldName(optional)] [Action]
[ 1 ]             [2]                      [3]                      [4]                    

[1] 所有的存储过程必须有前缀“proc_”，所有的系统存储过程都有前缀“sp_”。

注释：假如存储过程以sp_ 为前缀开始命名那么会运行的稍微的缓慢，这是因为SQL Server将首先查找系统存储过程。

[2] 表名就是存储过程主要访问的对象。

[3] 可选字段名就是条件子句。比如： proc_UserInfoByUserIDSelect

[4] 最后的行为动词就是存储过程要执行的任务。

如果存储过程返回一条记录那么后缀是：Select
如果存储过程插入数据那么后缀是：Insert
如果存储过程更新数据那么后缀是：Update
如果存储过程有插入和更新那么后缀是：Save
如果存储过程删除数据那么后缀是：Delete
如果存储过程更新表中的数据 (ie. drop and create) 那么后缀是：Create
如果存储过程返回输出参数或0，那么后缀是：Output
~~~

# SQL Server 的最大容量规范
https://docs.microsoft.com/zh-cn/sql/sql-server/maximum-capacity-specifications-for-sql-server?redirectedfrom=MSDN&view=sql-server-ver15