# 表值函数

## 定义
~~~sql
create function <函数名称>
(
    [
        @xxx 数据类型 [=默认值]
    ]
)
returns table
as
return
(
    <Query>
)

go

select * from dbo.<函数名称>()
~~~

## 使用表值函数
~~~sql
create function fn_T_SelectInfo
(

)
returns table
as
return
(
	select * from StudentInfo
)

go
select * from fn_T_SelectInfo()
~~~

## 使用公用表达式
~~~sql
create function fn_T_Select
(

)
returns table 
as
return
(
	with CTE_A
	as
	(
		select * from StudentInfo where Id between 1 and 15
	)
	select * from CTE_A
)
go

select * from dbo.fn_T_Select()
~~~


# 目前主流的数据库

## 关系型数据库

**商业**

1. Oracle
2. SqlServer

**开源数据库**

1. Mysql
2. MariDb
3. PostgreSQL

## 非关系型数据库 NoSql

1. Redis
2. MongoDb

</br>

# 函数
~~~
标量函数： 返回值是一个标量值（基础数据类型的值）
表值函数： 返回值是一个表（集合）
~~~